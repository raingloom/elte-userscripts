#include <u.h>
#include <libc.h>
#include <bio.h>
#include "./data.h"

Biobuf bstdin, bstdout;

char *line=0;
char *fieldsbuf=0;
char **fields=0;
u64int nfields=0;

int
eq(char *a, char *b)
{
	return strcmp(a, b) == 0;
}

u64int
field_name_to_index(char *name)
{
	u64int i;
	
	for (i=0; i<sizeof(timetable_headers); i++)
		if (eq(timetable_headers[i], name))
			return i;
	fprint(2, "invalid column name: %s", name);
	exit(-1);
}

char *
field(char *name)
{
	u64int i = field_name_to_index(name);
	return fields[i];
}

int
test_fields(void) {
return \
