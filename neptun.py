from IPython import embed as repl

import selenium.webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, ElementClickInterceptedException
from selenium.webdriver.common.by import By
import sys
import io
import os

# disables writing stuff to the files
devnull = False

browser = selenium.webdriver.Firefox()

delay = 30

def force_click(element):
	return selenium.webdriver.ActionChains(browser).move_to_element(element).click(element).perform()

def by_id(id):
	return browser.find_element_by_id(id)

def wait_by_id(id):
	return WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.ID, id)))

def wait_by_class(id):
	return WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.CLASS_NAME, id)))

def wait_until_stale(el):
	WebDriverWait(browser, delay).until(EC.staleness_of(el))

def escape(c):
	if c == '\t':
		return '\\t'
	elif c == '\n':
		return '\\n'
	elif c == '"':
		return '\\"'
	else:
		return c

def quote(s):
	return '"' + "".join([escape(c) for c in s]) + '"'

file_has_header=set()

def write(l, file=sys.stdout):
	if devnull:
		return
	print(*(map(quote, l)), sep='\t', file=file)

def write_header(l, file=sys.stdout):
	if not file in file_has_header:
		write(l, file=file)
		file_has_header.add(file)

def current_pagerlink_elem():
	return wait_by_class("pagerlink_disabled")

def other_pagerlink_elems():
	return browser.find_elements_by_class_name("pagerlink")

def maxpage():
	return (max(map(lambda e:int(e.text), [current_pagerlink_elem()] + other_pagerlink_elems())))

def getpage(p):
	if p != int(current_pagerlink_elem().text):
		e=list(filter(lambda e:int(e.text)==p, other_pagerlink_elems))[0]
		e.click()
		wait_until_stale(e)

subject_header_file = io.open("neptun-subject-head.tsv", mode="w")
course_body_file = io.open("neptun-body.tsv", mode="w")
attendees_file = io.open("attendees.tsv", mode="w")

browser.get("https://hallgato.neptun.elte.hu")

username = os.environ['NEPTUN_USERNAME']
password = os.environ['NEPTUN_PASSWD']

by_id("pwd").send_keys(password)
by_id("user").send_keys(username)
by_id("btnSubmit").click()

try:
	WebDriverWait(browser, 3).until(EC.alert_is_present(), "no alert present")
	alert = browser.switch_to.alert
	alert.accept()
except TimeoutException:
	pass

wait_by_id("mb1_Targyak").click()
wait_by_id("mb1_Targyak_Targyfelvetel").click()
wait_by_id("upFilter_expandedsearchbutton").click()

for study_plan in ("upFilter_rbtnSubjectType_0", "upFilter_rbtnSubjectType_1"):
	study_plan_e = wait_by_id(study_plan)
	try:
		study_plan_e.click()
	except selenium.common.exceptions.ElementClickInterceptedException as exc:
		WebDriverWait(browser, 20).until(EC.element_to_be_clickable((By.ID, study_plan)))
	wait_until_stale(study_plan_e)
	
	for p in range(1,maxpage()+1):
		getpage(p)
				
		subj_table = wait_by_id("h_addsubjects_gridSubjects_bodytable")
		
		column_names = list(map(lambda x: x.text, subj_table.find_element_by_tag_name(
			"thead").find_elements_by_tag_name("th")))
		
		write_header(column_names, file=subject_header_file)
				
		subject_name_column = column_names.index("Tárgy neve")
		subject_id_column = column_names.index("Tárgy kódja")
		
		rows = subj_table.find_element_by_tag_name(
			"tbody").find_elements_by_tag_name("tr")
		
		course_header = None
		
		for row0 in rows:
			row_elements = row0.find_elements_by_tag_name("td")
			row_elements_text = list(map(lambda x:x.text, row_elements))
			write(["subject"] + row_elements_text, file=course_body_file)
			class0 = row_elements[subject_name_column]
			class_name = class0.text
			class_id = row_elements[subject_id_column].text
			class0.find_element_by_tag_name("span").click()
			courses_table = wait_by_id(
				"Addsubject_course1_gridCourses_bodytable")
			course_columns = ["Tárgy kódja", "Tárgy neve"] + list(map(lambda x: x.text, courses_table.find_element_by_tag_name(
				"thead").find_elements_by_tag_name("th")))
			
			if course_header == None:
				course_header = course_columns
			else:
				assert course_header == course_columns
			
			for course_row_i in range(len(courses_table.find_element_by_tag_name("tbody").find_elements_by_tag_name("tr"))):
				# need to re-run the query because otherwise we get stale elements after descending into the attendee table
				course_row = list(map(lambda r:r.find_elements_by_tag_name("td"), courses_table.find_element_by_tag_name("tbody").find_elements_by_tag_name("tr")))[course_row_i]
				course_link = course_row[1].find_element_by_class_name("link")
				course_link.click()
				wait_by_id('__tab_Course_data1_ctl01').click()
				for attendee_row in wait_by_id('Students1_gridStudents_bodytable').find_element_by_tag_name('tbody').find_elements_by_tag_name('tr'):
					write([class_name, class_id] + list(map(lambda e:e.text, attendee_row.find_elements_by_tag_name('td'))), file=attendees_file)
				cancel = wait_by_id('upFunction_h_addsubjects_upModal_upmodal_subjectdata_ctl02_Subject_data_for_schedule_upParent_tab_ctl00_upAddSubjects_Addsubject_course1_upModals_upmodal_coursedata_upFootermodal_coursedata_footerbtn_modal_coursedata_Vissza')
				cancel.click()
				wait_until_stale(cancel)
				
				write(["course", class_name, class_id] + list(map(lambda x:x.text, course_row)), file=course_body_file)
			cancel = by_id("function_cancel1")
			cancel.click()
			wait_until_stale(cancel)

course_header_file = io.open("neptun-course-head.tsv", mode="w")
write(course_header, file=course_header_file)
course_header_file.flush()
course_header_file.close()
