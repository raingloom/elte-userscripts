--[[
io.stdout:setvbuf 'no'
io.stderr:setvbuf 'no'
--]]
local inspect = require("inspect")

local function parseTsvFile(file)
  local lines = file:lines()
  local colnums = {}
  local colnames = {}
  local header_line = assert(lines(), "no header line")
  local i = 1
  for col in header_line:gmatch("[^\t]*") do
    colnames[i], i = col, i+1
  end
  local ret, j = {}, 1
  for line in lines do
    local l, k = {}, 1
    ret[j], j = l, j+1
    for col in line:gmatch("[^\t]*") do
      l[colnames[k]], k = col, k+1
    end
  end
  for k,v in pairs(colnames) do
    colnums[v] = k
  end
  ret.colnums = colnums
  ret.colnames = colnames
  return ret
end

local function group_by(lines, k1)
  local ret = {}
  for _, line in ipairs(lines) do
    local t = ret[line[k1]]
    if t == nil then
      t = {}
      ret[line[k1]] = t
    end
    table.insert(t, line)
  end
  return ret
end

local function hierarchy(db, k1, ...)
  

local timetable = parseTsvFile(assert(io.open("timetable.tsv", "r")))
local neptun_courses = parseTsvFile(assert(io.open("neptun-courses.tsv", "r")))
print(inspect(hierarchy(neptun_courses, [["Tárgy neve"]], [["Tárgy kódja"]], [["Kurzus kódja"]])))
