	#include <u.h>
	#include <libc.h>
	#include <bio.h>
	#include "./data.h"
	
	Biobuf bstdin, bstdout;
	
	char *line=0;
	char *fieldsbuf=0;
	char **fields=0;
	u64int nfields=0;
	
	int
	eq(char *a, char *b)
	{
		return strcmp(a, b) == 0;
	}
	
	u64int
	field_name_to_index(char *name)
	{
		u64int i;
		
		for (i=0; i<sizeof(timetable_headers); i++)
			if (eq(timetable_headers[i], name))
				return i;
		fprint(2, "invalid column name: %s", name);
		exit(-1);
	}
	
	char *
	field(char *name)
	{
		u64int i = field_name_to_index(name);
		return fields[i];
	}
	
	int
	test_fields(void) {
	return \
	eq(field("Kurzusnev"), "Algoritmusok és adatszerkezetek 1. EA (BSc,08,B)") || \
	eq(field("Kurzusnev"), "Algoritmusok és adatszerkezetek 1. GY. (BSc, 08, B") || \
	eq(field("Kurzusnev"), "Analízis 1. EA. (BSc,08,B)") || \
	eq(field("Kurzusnev"), "Analízis 1. GY (BSc,08,B)") || \
	eq(field("Kurzusnev"), "Diszkrét matematika 2. EA. (BSc,08,B)") || \
	eq(field("Kurzusnev"), "Diszkrét matematika 2. GY. (BSc,08,B)") || \
	eq(field("Kurzusnev"), "Formális nyelvek és automaták EA (BSc, 08,B)") || \
	eq(field("Kurzusnev"), "Formális nyelvek és automaták GY (BSc,08,B)") || \
	eq(field("Kurzusnev"), "Programozás módszertani alapjai 2. EA. (BSc,08,B)") || \
	eq(field("Kurzusnev"), "Programozás módszertani alapjai 2. GY. (BSc,08,B)") || \
	eq(field("Kurzusnev"), "Programozási nyelvek I. C++ EA+GY. (BSc 08, B)") || \
	eq(field("Kurzusnev"), "Funkcionális programozási nyelvek 2.") || \
	eq(field("Kurzusnev"), "Haladó Haskell (EC)") || \
	0;
	}/* test_fields */
	
	void
	split_line(void)
	{
		u64int i, j, len = strlen(line);
		
		nfields=0;
		
		fieldsbuf = realloc(fieldsbuf, len * sizeof(char));
		memcpy(fieldsbuf, line, len);
		
		for (i=j=0; i<=len; i++) {
			switch (fieldsbuf[i]) {
			case '\t':
			case '\0':
				fieldsbuf[i] = '\0';
				nfields++;
				fields = realloc(fields, sizeof(char *) * nfields);
				fields[nfields-1] = fieldsbuf + j;
				j=i+1;
			}
		}
		
		assert(nfields-1 == sizeof(timetable_headers) / sizeof(char *));
	}
	
	void
	string_fields(void)
	{
		u64int i, j, o;
		char *field, c;
		
		for (i=0; i<nfields; i++) {
			field=fields[i];
			for (j=o=0; field[j]!='\0'; j++) {
				if (field[j] == '\\') {
					switch(field[j+1]) {
					case '\\':
						c='\\';
						break;
					case '"':
						c='"';
						break;
					case 't':
						c='\t';
						break;
					case 'n':
						c='n';
						break;
					default:
						fprint(2, "invalid escape: \\%c", field[j+1]);
						exit(-1);
					}
					o++;
					j++;
				} else {
					c=field[j];
				}
				field[j-o]=c;
			}
			fprint(2, "\"%s\"\n", field);
			if (*field != '\0') {
				assert(field[j-1] == '"');
				field[j-1]='\0';
				/* trim beginning and ending quotes. yes, those are mandatory */
				assert(field[0] == '"');
				fields[i]++;
			}
		}
	}
	
	void
	main()
	{
		Binit(&bstdin, 0, OREAD);
		Binit(&bstdout, 1, OWRITE);
		while (line = Brdstr(&bstdin, '\n', 0)) {
			print("%s\n", line);
			split_line();
			string_fields();
			if (test_fields())
				Bprint(&bstdout, "%s", line);
			Bflush(&bstdout);
			free(line);
		}
		exit(0);
	}
