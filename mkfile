<$PLAN9/src/mkhdr
MKSHELL=rc

data.h: timetables/header.tsv
	@{
		echo 'char *timetable_headers[]={'
		sed -e 's/	/, /g' -e 's/^/	/g' timetables/header.tsv
		echo '};'
	} > $target

candidates.tsv: timetables/body.tsv timetables/column_numbers classnames escape
	col='"Kurzusnev"'
	coln=`{grep $col timetables/column_numbers | awk -F'	' '{print $1}'}
	@{
		ifs=''
		while (line=`{read}) {
			ifs='	'
			spl=`{echo -n $line}
			ifs=''
			pattern='^' ^ `{echo -n $spl($coln) | ./escape} ^ '$'
			if (grep $pattern classnames >/dev/null) {
				echo -n $line
			}
		}
	} <timetables/body.tsv >$target

candidates-proportional-body.html: candidates.tsv timetables/column_numbers timetables/header.tsv table-head.html table-foot.html print-settings
	. ./print-settings
	col='"Idopont"'
	coln=`{grep $col timetables/column_numbers | awk -F'	' '{print $1}'}
	@{
		origifs=$ifs
		cat timetables/header.tsv | cut -f $select -d '	' | ./tsv2html
		ifs=''
		while(line=`{read}) {
			ifs='	'
			fields=`{echo -n $line}
			ifs=''
			@{
				time=`{echo -n $fields($coln)}
				lua -e 'local bh, bm, eh, em = (' ^ $time ^ '):match("%a+ (%d+):(%d+)-(%d+):(%d+)");' ^\
					'assert(bh and bm and eh and em, "oh shit, wrong time format?");' ^\
					'local len = (eh*60+em) - (bh*60+bm);' ^\
					'local height = len // (60 / ' ^ $emPerHour ^ ');' ^\
					'io.write(string.format([[<tr style="height: %dem;">]], height));'
				echo -n $line | cut -f $select -d '	' | ./tsv2html
			} |  sed 's/<tr>//'
		}
		status=''
	} < candidates.tsv > $target

timetable.tsv: timetables/header.tsv timetables/body.tsv
	cat timetables/header.tsv timetables/body.tsv > $target

candidates-with-grid.html: candidates-proportional-body.html timetable-grid-body.html
	cat table-head.html \
		candidates-proportional-body.html \
		timetable-grid-body.html \
		table-foot.html > $target


timetable-grid-body.html: table-head.html table-foot.html print-settings mkfile 
	@{
		. ./print-settings
		height=`{echo 60/$emPerHour'*0.5' | bc}
		echo '<style>'
		echo '	td {'
		echo '		width: ' ^ $timetableColumnWidthEm ^ 'em;'
		echo '		text-align: left;'
		echo '		vertical-align: top;'
		echo '	}'
		echo '</style>'
		
		echo '<tr>'
		for (day in Monday Tuesday Wednesday Thursday Friday Saturday Sunday)
			echo '<th>'$day'</th>'
		echo '</tr>'
		
		seq 0 23 | while(hour=`{read}) {
			for (minute in 00 30) {
				echo -n '<tr style="height:' ^ $height ^ 'em;">'
				seq 7 | while(read >/dev/null) {
					echo -n '<td>'$hour':'$minute'</td>'
				}
			}
			echo '</tr>'
		}
		status=''
	} > $target

timetables/%:
	cd timetables/
	mk $MKFLAGS $stem

neptun-body.tsv: .env neptun.py
	cat .env | cut -f 2 -d '=' | python neptun.py | tee $target

neptun-%-head.tsv:V: neptun-body.tsv

neptun-data:V: neptun-body.tsv neptun-course-head.tsv neptun-subject-head.tsv

neptun-%s.tsv: neptun-data neptun-%-head.tsv
	cat neptun-$stem-head.tsv > $target
	grep '^"' ^ $stem ^ '"' neptun-body.tsv | cut -f 2- -d '	' >> $target

neptun-%s.html: neptun-%s.tsv tsv2html
	@{
		select='1,3,4,5,10,13,14'
		select=1-
		cat table-head.html
		cat neptun-$stem-head.tsv 'neptun-'$stem's.tsv' | cut -f $select -d '	' | ./tsv2html -H
		cat table-foot.html
	} > $target

zip: zip.c
	9 $CC zip.c
	9 $LD -o $target zip.$O

escape: escape.c
	9 $CC escape.c
	9 $LD -o $target escape.$O
